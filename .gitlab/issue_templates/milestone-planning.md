<!-- 
Title of the issues should be something like: Milestone Planning - 12.7
-->
## Milestone Planning - MILESTONE_NUMBER

This issue is to help coordinate planning for the upcoming milestone.

### Before Milestone Begins
* [ ] @dhershkovitch Create draft of issues to be included in this milestone. _Due: 4th of Month_
* [ ] **Engineers** Move any issues not going to be completed in previous milestone to this milestone. Be sure to include the  ~"to schedule" label and correct weight. See [Rescheduling Issues](https://about.gitlab.com/handbook/engineering/development/ops/monitor/apm/#rescheduling-issues) in the handbook. _Due: 12th of Month_
* [ ] @dhershkovitch Finalize list of issues. _Due: 13th of Month_
* [ ] @dhershkovitch Apply ~"release post item" label for top issues to highlight in kickoff call. _Due: 13th of Month_
* [ ] @mnohr Apply ~Deliverable label for any ~backend issues. _Due: 13th of Month_
* [ ] @@ClemMakesApps  Apply ~Deliverable label for any ~frontend issues. _Due: 13th of Month_
* [ ] @dhershkovitch Record and upload the kickoff call. _Due: 17th of Month_
* [ ] @mnohr Ensure all unfinished issues are moved out of the previous milestone. Be sure to include the  ~"to schedule" label and correct weight.  _Due: 18th of Month_

### During Milestone
* [ ] **All** Development on the milestone begins. _Starts: 18th of Month_

### End of Milestone
* [ ] @dhershkovitch Create milestone release post MRs. _Due: 16th of Month_
* [ ] @mnohr Review and merge milestone release post MRs. _Due: 17th of Month_

### References
* [Product Development Timeline](https://about.gitlab.com/handbook/engineering/workflow/#product-development-timeline)

/assign @dhershkovitch @mnohr @@ClemMakesApps
/label ~"group::apm" ~"devops::monitor"
