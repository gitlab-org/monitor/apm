<!-- 
Title should be:

APM ##.# Planning issue
-->

### APM Issue Boards

* [APM Planning Board](https://gitlab.com/groups/gitlab-org/-/boards/1065731)
* [APM Next Milestone Board](https://gitlab.com/groups/gitlab-org/-/boards/1593380)
* [APM Current Milestone Board](https://gitlab.com/groups/gitlab-org/-/boards/1509668)

### Goals for the milestone:

<!-- Replace these with the high-level goals for the milestone -->
*  Goal 1...
*  Goal 2...

### SRE Shadow Program

1 week dedicated time for the [SRE Shadow Program](https://about.gitlab.com/handbook/engineering/development/ops/monitor/#sre-shadow-program)

<!-- Add any engineer(s) who will be participating in the SRE Shadow program -->
*  Participant 1
*  Participant 2

### Additional comments



### Scope of Work for Engineering

<!--
List of issues in priority order go in this table
Use :white_check_mark: for Frontend and/or Backend if there is work for that team
-->

| Priority | Issue | Category | Notes | Frontend | Backend |
|----------|-------|----------|-------|----------|---------|
| 1        |       |          |       |          |         |
| 2        |       |          |       |          |         |

### Scope of Work for UX

<!-- What work will UX be focused on? -->

| Issue | When it should be ready |
|-------|-------------------------|
|       |                         |

### Scope of Work for Testing

<!-- What work will Testing be focused on? -->

| Issue | Investigates/Tests | Due on |
|-------|--------------------|--------|
|       |                    |        |

/label ~"section::ops" ~"Planning Issue" 
/assign @ClemMakesApps @dhershkovitch @mnohr 
