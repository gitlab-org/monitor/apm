<!-- 
Title should be:

APM ##.# Release Post Checklist
-->
## Release Posts

| Feature | Engineer(s) | Engineering Completed? | Docs URL | Release Post |
|---------|-------------|------------------------|----------|--------------|
|         |             |                        |          |              |

## Merged Release Posts

| Feature | Engineer(s) | Engineering Completed? | Docs URL | Release Post |
|---------|-------------|------------------------|----------|--------------|
|         |             |                        |          |              |

## Move to next milestone

| Feature | Engineer(s) | Engineering Completed? | Docs URL | Release Post |
|---------|-------------|------------------------|----------|--------------|
|         |             |                        |          |              |

## Links

<!-- Replace the milestone_title in the URL with the correct mileston -->

- [List of Release Post merge requests](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=release%20post%20item&label_name[]=group%3A%3Aapm&milestone_title=13.2)
- [Issues with `release post item` label](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aapm&label_name[]=release%20post%20item&milestone_title=13.2)
